const fs = require('fs');

const functions = {
    response: (res) => {
        return {
            apiResponse: {
                ...res
            }
        }
    },
    renameFile: (oFile, nFile, res) => {
        fs.rename(oFile, nFile, function(err) {
            if (err) throw res.json(response({ error: `upload file is fail`}));
            fs.unlink(oFile, function() {
                if (err) throw res.json(response({ error: `upload file is fail`}));
                res.json(functions.response({ success: true }));
            });
        });
    },
    getFileType: (fileName) => {
        return fileName ? fileName.split('.').pop() : 'jpg';
    },
    is_File: (filePath) => {
        return !!fs.existsSync(filePath);
    },
    remove_file: (filePath) => {
        return fs.unlinkSync(filePath);
    }
}

module.exports = functions;