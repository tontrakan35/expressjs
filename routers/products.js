const express = require('express');
const multer = require('multer');
const moment = require('moment');
const { response, renameFile, is_File, remove_file } = require('../functions/general');
const upload = multer({ dest: 'public/images/products' });
const knex = require('../db_connect')
const route = express.Router();

route.post('/load', (req, res) => {
  const dest = `public/images/products/`;

  knex('products').select('*').then((items) => {
    
    if(!items?.length) {
      res.json(response({ error: `not Found`, data: [] }));
      return false
    }
    
    const results = items.map((item) => {
      return {...item, image: `${dest}${item.image}`}
    })

    res.json(response({ 
      data: [...results],
      sucess: true
    }))

  }).catch((err) => {
    res.json(response({ error: err?.sqlMessage }))
  })
})

route.post('/load/:id', (req, res) => {
  const dest = `public/images/products/`;

  knex('products').select('*')
    .where('id', req.params.id)
    .first()
    .then((items) => {
    if(!items) {
      res.json(response({ error: `not Found`, data: null }));
      return false
    }

    res.json(response({ 
      data: {...items, image: `${dest}${items.image}`},
      sucess: true
    }))

  }).catch((err) => {
    res.json(response({ error: err?.sqlMessage }))
  })
})

route.post('/save', upload.single('file'), (req, res) => {
  const dest = `${req?.file?.destination}/`;
  const old_path = req?.file ? dest + req?.file?.filename : '';
  const new_path = dest + req.body.image;

  const params = {
    ...req.body,
    createDt: moment().format('YYYY-MM-DD hh:mm:ss'),
    lastUpdate: moment().format('YYYY-MM-DD hh:mm:ss')
  }


  knex('products').insert(params).then((result) => {
    if (!result) {
      res.json(response({ error: `save the data is fail` }));
      return false
    }

    if (req?.file) {
      renameFile(old_path, new_path, res);
      return true;
    }

    res.json(response({ sucess: true }));

  }).catch((err) => {
    res.json(response({ error: err?.sqlMessage }))
  });

  // res.json(response)
})

route.post('/save/:id', upload.single('file'), (req, res) => {
  const dest = `public/images/products/`;
  let old_path = req?.file ? dest + req?.file?.filename : '';
  const new_path = dest + req.body.image;

  const params = {
    ...req.body,
    lastUpdate: moment().format('YYYY-MM-DD hh:mm:ss')
  }

 knex('products').select('image')
    .where('id', req.params.id)
    .first()
    .then((item) => {
      
      if(!item) {
        res.json(response({ error: `not found` }));
        old_path && remove_file(old_path);
        return false;
      };

      const isFile = is_File(`${dest}${item.image}`);
        // console.log(is_File(`${dest}${item.image}`), `${dest}${item.image}`);
      if(isFile && !old_path) {
        old_path = `${dest}${item.image}`;
      } else {
        isFile && remove_file(`${dest}${item.image}`);
      }

      knex('products').where('id', req.params.id).update(params).then((result) => {
          if(!result) {
              res.json(response({ error: `save the data is fail`}));
              return false
          } 

          if(old_path) {
              renameFile(old_path, new_path, res);
              return true;
          }

          res.json(response({ sucess: true}));

      }).catch((err) => {
          res.json(response({ error: err?.sqlMessage}))
      });
    }).catch(() => {
      res.json(response({ error: err?.sqlMessage }))
    });




  // res.json(response)
})

route.post('/remove/:id', (req, res) => {
  
  const dest = `public/images/products/`;

 knex('products').select('image')
    .where('id', req.params.id)
    .first()
    .then((item) => {

      if(!item) {
        res.json(response({ error: `not found` }));
        return false;
      };

      const isFile = is_File(`${dest}${item.image}`);
        // console.log(is_File(`${dest}${item.image}`), `${dest}${item.image}`);
      if(isFile) {
        remove_file(`${dest}${item.image}`);
      }
      
      knex('products').where('id', req.params.id).del().then((result) => {
          if(!result) {
              res.json(response({ error: `remove is fail`}));
              return false
          } 

          res.json(response({ sucess: true}));

      }).catch((err) => {
          res.json(response({ error: err?.sqlMessage}))
      });
    });
})

module.exports = route;