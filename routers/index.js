const express = require('express');
const route = express.Router();

route.use('/products', require('./products'));
route.use('/services', require('./services'))

module.exports = route;