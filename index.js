const express = require('express'); 
const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(express.raw({ type: 'application/form-data' }));

app.use('/api', require('./routers/index'));

app.get('/public/images/:folder/:image', (req, res) => {
    const imagename = req.params.image;
    const folder = req.params.folder;
    res.sendFile(`public/images/${folder}/${imagename}`, {root: __dirname});
});

app.listen(1188, () => {
    console.log(`http://localhost:1188`)
})